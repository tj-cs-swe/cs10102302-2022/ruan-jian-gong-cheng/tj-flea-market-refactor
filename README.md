# TJ-flea-market

同济跳蚤市场课程项目

## 运行环境说明

Windows10

python 版本 3.10.0

依赖库见 requirements.txt

安装方法：

```
conda create -n tj_market python==3.10.0
conda activate tj_market
pip install -r requirements.txt
或
conda env create --file environment.yml
```

## 使用说明

```

python main.py
```
