#返回值规范

#flask
from flask_socketio import emit, join_room, leave_room
from flask_login import current_user, login_user, logout_user, login_required
from flask import make_response, request, jsonify, render_template, flash, redirect, url_for
#enum
from user.models import User_state
from admin.models import Feedback_kind, Feedback_state
from order.models import Order_state
#model
from user.models import User
from admin.models import Feedback, User_Management
from order.models import Contact, Review, Order, Order_State_Item, Order_Item
from item.models import Item, History, Favor, Item_type, Item_state
from chat.models import Room, Message, Recent_Chat_List

from chat.routes import create_or_update_meet_list
#common
import copy
import os
import re
import json
import datetime
import time
import random
from werkzeug.security import generate_password_hash
'''
statusCode:
•	200：操作成功返回。
•	201：表示创建成功，POST 添加数据成功后必须返回此状态码。
•	400：请求格式不对。
•	401：未授权。（User/Admin）
•	404：请求的资源未找到。
•	500：内部程序错误。

其他详见接口文档
'''
SYS_ADMIN_NO = 80000000

default_res = {'success': True, 'statusCode': 200, 'message': '', 'data': {}}


def make_response_json(statusCode: int = 200,
                       message: str = "",
                       data: dict = {},
                       success: bool = None,
                       quick_response: list = None):
    '''
    :params quick_response: [statusCode（若为0，则自动改为200）, message]
    如果success未指定，则当statusCode==200时为True，否则False
    '''
    if type(quick_response) == list and len(quick_response) == 2:
        statusCode = quick_response[0]
        if statusCode == 0:
            statusCode = 200
        message = quick_response[1]
    if success == None:
        success = True if statusCode // 100 == 2 else False
    return make_response(jsonify({'success': success, 'statusCode': statusCode, 'message': message, 'data': data}))

def createPath(path: str) -> None:
    if not os.path.exists(path):
        os.makedirs(path)
    elif not os.path.isdir(path):
        os.remove(path)
        os.makedirs(path)

def send_message(sender: str | int, receiver: str | int, message: str, type: int = 0):
    '''
    直接从后端发送消息
    :params type:0为文本,1为图片
    '''
    try:
        sender = str(sender)
        receiver = str(receiver)

        create_or_update_meet_list(sender, receiver)
        create_or_update_meet_list(receiver, sender)

        time = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        room = sender + '-' + receiver
        reroom = receiver + '-' + sender
        if (sender != receiver):
            roomid = Room.get_or_none(Room.room_id == room)
            reroomid = Room.get_or_none(Room.room_id == reroom)
            if (roomid == None and reroomid == None):
                Room.create(room_id=room, last_sender_id=sender)
            elif reroomid != None:
                room = reroom

        state = Room.get_or_none(Room.room_id == room)

        read = 0
        if (state == None):
            pass
        elif state.room_state == 2:
            read = 1

        if read == 0:
            if Recent_Chat_List.get_or_none(receiver_id=receiver) == None:
                Recent_Chat_List.create(receiver_id=receiver,
                                        sender_id=sender,
                                        last_time=time,
                                        last_msg=message,
                                        unread=1)
            else:
                update_data = {"last_time":time,"last_msg":message,"unread":Recent_Chat_List.unread+1}
                condition = [(Recent_Chat_List.receiver_id == receiver)&(Recent_Chat_List.sender_id == sender)]
                Recent_Chat_List.update_with_condition(update_data,condition)
                update_data.pop("unread")
                tep_condtion = [(Recent_Chat_List.receiver_id == sender)&(Recent_Chat_List.sender_id == receiver)]
                Recent_Chat_List.update_with_condition(update_data,tep_condtion)

        Message.create(msg_time=time,
                       room_id=room,
                       sender_id=sender,
                       msg_type=type,
                       msg_content=message,
                       msg_read=read)
        room_data = {"last_message":message,"last_sender_id":sender,"msg_type":type}
        condition = [Room.room_id == room]
        Room.update_with_condition(room_data,condition)


        emit('message', {
            'sender': sender,
            'msg': message,
            'other_user': receiver,
            'time': time,
            'type': type
        },
            room=sender,
            namespace='/chat')

        if (read==1):
            emit('message', {
                'sender': sender,
                'msg': message,
                'other_user': sender,
                'time': time,
                'type': type
            },
                room=receiver,
                namespace='/chat')
        else:
            emit('notice', {
                'sender': sender,
                'msg': message,
                'other_user': sender,
                'time': time,
                'type': type
            },
                room=receiver,
                namespace='/chat')
        return (200, "操作成功")
    except Exception as e:
        return (500, repr(e))

from functools import wraps

def verify_login(func):
    """
    装饰器，用于验证登录
    """

    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if current_user and (not current_user.is_authenticated):
            return make_response_json(401, "该用户未通过验证")

        return func(*args, **kwargs)
    return wrapped_function

@verify_login
def verify_admin(func):
    """
    装饰器，用于管理员鉴权（同时验证登录）
    """

    @wraps(func)
    def wrapped_function(*args, **kwargs):
        if current_user.state < User_state.Admin.value:
            return make_response_json(401, "权限不足")
        return func(*args, **kwargs)

    return wrapped_function
