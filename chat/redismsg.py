from redis import Redis
import json

REDIS_DB = Redis(host="127.0.0.1", port=6379, db=5)


def set_redis(receiver, sender):
    receiver_msg = REDIS_DB.get(receiver)
    if receiver_msg:
        receiver_msg = json.loads(receiver_msg)
        if receiver_msg.get(sender):
            receiver_msg[sender] += 1
        else:
            receiver_msg[sender] = 1
    else:
        receiver_msg = {sender: 1}
    REDIS_DB.set(receiver, json.dumps(receiver_msg))


def get_redis_one(receiver, sender):
    receiver_msg = REDIS_DB.get(receiver)
    if receiver_msg:
        receiver_msg = json.loads(receiver_msg)
        count = receiver_msg.get(sender, 0)
        receiver_msg[sender] = 0
        REDIS_DB.set(receiver, json.dumps(receiver_msg))
        return count
    else:
        return 0